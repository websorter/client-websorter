import configparser
import requests
import sys

conf = configparser.ConfigParser()
conf.read("./.env")

if len(sys.argv) > 1:
    url = conf.get("variables", "API_URL") + sys.argv[1]
else:
    url = conf.get("variables", "API_URL")


print("je vais GET " + url)

try:
    request = requests.get(url=url, params={})
except (ConnectionRefusedError, ConnectionError):
    print("Impossible de se connecter à l'API")
    exit()

print(request.json())