# Runner WebSorter

Python based runner for the WebSorter project.
WebSorter is a project carried out as part of our studies at ESGI.
If you want to see more about this project click [here](https://gitlab.com/websorter).

Don't forget to setup project variables in the .env file and install requirements with :
```shell script
  pip install -r requirements.txt
```

## Run application
```shell script
  py Runner.py --login userLogin --password userPassword --seconds seconds
``` 

- seconds = time the runner should be running for (in seconds)

## Run unit tests
```shell script
  py -m unittest
```
