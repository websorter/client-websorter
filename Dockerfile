FROM python:3
COPY . /app
WORKDIR /app

ARG SECONDS
ARG LOGIN
ARG PASSWORD

RUN pip install -r requirements.txt

CMD python -u ./Runner.py --login "${LOGIN}" --password "${PASSWORD}" --seconds ${SECONDS}