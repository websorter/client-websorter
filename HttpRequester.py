import requests

default_headers = {"content-type": "application/json"}


def request(method, url, params={}, headers={}):
    if method == "get":
        default_headers.update(headers)
        return requests.get(url, params, headers=default_headers)
    elif method == "post":
        return requests.post(url, params, headers=default_headers)
    elif method == "put":
        return requests.put(url, params, headers=default_headers)
    elif method == "delete":
        return requests.delete(url, params)
    else:
        print("Wrong http method : " + method)
        return None
