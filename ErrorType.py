from enum import Enum


class ErrorType(Enum):
    BadRequest = 'Bad response status code'
    Redirected = 'Got redirected'
    Unidentified = 'Failed to identify'

    @staticmethod
    def is_error(object):
        return isinstance(object, ErrorType)