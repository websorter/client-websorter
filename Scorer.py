import re
from urllib.parse import urlparse


class Scorer:
    def __init__(self, html_code, url, subject):
        self.html_code = html_code
        self.parsed_url = urlparse(url)
        self.subject = subject

    def evaluate(self, strong_keywords, keywords):
        score = 0
        for each_strong_keywords in strong_keywords:
            if re.search(each_strong_keywords, self.subject, re.IGNORECASE):
                # print('+ 35 ', each_strong_keywords)
                score += 35
                break
        for each_strong_keywords in strong_keywords:
            if re.search(each_strong_keywords, self.parsed_url.geturl(), re.IGNORECASE):
                # print('+ 30 ', each_strong_keywords)
                score += 30
                break
        for each_keyword in keywords:
            if re.search(each_keyword, self.html_code, re.IGNORECASE):
                # 14 keywords x 2.5 = 35
                # print('+ 2.5 ', each_keyword)
                score += 2.5
        # print('score = ', score)
        return score
