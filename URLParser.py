import logging
import sys

import HttpRequester
import re
from bs4 import BeautifulSoup

from DocumentType import DocumentType
from ErrorType import ErrorType
from Scorer import Scorer


class DocumentFinder:
    def __init__(self, task, subject):
        self.task = task
        self.subject = subject
        self.identification_functions = {DocumentType.WelcomePage: look_for_home_page,
                                         DocumentType.SignUpPage: look_for_sign_up_page,
                                         DocumentType.LoginPage: look_for_login_page,
                                         DocumentType.ProfilePage: look_for_profile_page,
                                         DocumentType.ECommercePage: look_for_ecommerce_page,
                                         DocumentType.ArticlesPage: look_for_articles_page,
                                         DocumentType.StreamPage: look_for_stream_page,
                                         DocumentType.InformationPage: look_for_information_page,
                                         DocumentType.ContactPage: look_for_contact_page,
                                         DocumentType.LegalDisclaimerPage: look_for_legal_disclaimer_page,
                                         DocumentType.PaymentPage: look_for_payment_page}
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    def identify(self):
        print("I'm going to identify this web page : " + self.task.url)

        response = HttpRequester.request("get", self.task.url)
        check_task = self.check_response(response)
        if check_task is not True:
            print("Aborting identification operation...")
            return check_task

        html_code = response.text
        soup = BeautifulSoup(html_code, features="html.parser")
        self.subject[0] = self.find_page_subject(soup)

        # print(html_code)

        scorer = Scorer(html_code, self.task.url, self.subject[0])
        max_score = 0
        best_match = DocumentType.UnidentifiedPage

        for document_type, identification_function in self.identification_functions.items():
            result, score = identification_function(scorer)
            logging.info(document_type.value)
            logging.info('score = ' + str(score))
            if score > max_score:
                max_score = score
                best_match = document_type
        print('best match : ')
        print(best_match.value)
        print('with score : ' + str(max_score))
        if max_score <= 10:
            print('score too low to be meaningful, returning UnidentifiedPage')
            return DocumentType.UnidentifiedPage
        return best_match

    def check_response(self, response):
        # if response.url != self.task.url:
        #     print("got redirected from " + self.task.url + " to " + response.url)
        #     return ErrorType.Redirected
        if response.status_code != 200:
            print("got response status code " + str(response.status_code))
            return ErrorType.BadRequest
        return True

    def find_page_subject(self, soup):
        results = soup.find('title')
        if results is not None:
            for p in results:
                print('page title : ' + p)
                return p
        else:
            return 'Not found subject'


def look_for_home_page(scorer):
    if scorer.parsed_url.path == "/":
        # print("It's a home page since there is no path after the site name")
        return True, 100
    return False, 0


def look_for_sign_up_page(scorer):
    strong_keywords = ['signup', 'inscription', 'subscribe', 'register', 'join']
    keywords = ['inscription', 's\'inscrire', 'email', 'login', 'confirmation', 'mot de passe', 'naissance',
                'date', 'acceptez', 'déjà un compte', 'name', 'compte', 'genre', 'conditions generales',
                'Politique d’utilisation des données', 'inscrivez-vous avec', 'avec facebook', 'avec google',
                'avec apple', 'avec google+', 'e-mail', 'adresse e-mail', 'username', 'pseudo', 'addresse', 'carte bancaire',
                'Confirmez votre adresse e-mail', 'Confirmez votre mot de passe', 'date de naissance', 'sexe', 'je ne suis pas un robot',
                'politique de confidentialités', 'conditions générales d\'utilisation', 'bouton d\'inscription', 'partagez mes données',
                'register', 'Inscription rapide', 'Inscription rapide et gratuite', 'Nom complet', 'Confirmer le mot de passe',
                'Créer votre compte', 'déjà un compte ?', 'm\'envoyer d\'emails', 'continuer avec google', 'continuer avec facebook', 'continuer avec microsoft',
                'conectez-cous', 'adresse e-mail', 'join', 'create your account', 'Email address', 'Verify your account', 'at least 8 characters',
                'create account']

    score = scorer.evaluate(strong_keywords, keywords)

    if score >= 25:
        return True, score
    return False, score


def look_for_login_page(scorer):
    strong_keywords = ['signin', 'login', 'connexion', 'se connecter']
    keywords = ['connexion', 'Connexion', 'Se connecter', 'login', 'identifiant', 'mot de passe', 'username',
                'password',
                'valider', 'S\'inscrire', 'pas de compte', 'account', 'Ajouter', 'remember me', 'se souvenir de moi',
                'mot de passe oublié', 'Forgot your password', 'with google', 'with facebook', 'with apple',
                'oublié votre mot de passe ?',
                'codes d\'accès oubliés', 'Espace client', 'connexion via twitter', 'connexion via google',
                'connexion via office 365',
                'email', 'votre compte', 'Sign In', 'email', 'Sign in to your account']
    score = scorer.evaluate(strong_keywords, keywords)
    if score >= 50:
        return True, score
    return False, score


def look_for_profile_page(scorer):
    strong_keywords = ['profil', 'user', 'compte' , 'gitlab', 'abonnements', 'abonnés', 'tweets', 'publications']
    keywords = ['account', 'profil', 'infos', 'centre d\’intérêt', 'Messagerie',
                'description', 'abonnés', 'relations', 'prénom', 'nom', 'tableau de bord',
                'Ajouter une section', 'facebook', 'github', 'linkedin', 'coordonnées', 'expérience',
                'votre réseau', 'profil public', 'profil privé', 'Publications','évènements', 'photos',
                'partager', 'envoyer un message','communauté', 'publications', 'abonnements', 'rechercher',
                'tweet', 'tweets', 'suivre', 'vues', 'j\'aime', 'Vous pourriez aimer', 'Tendances pour vous', 'masquer',
                'bloquer l\'utilisateur', 'signaler', 'débloquer', 'a rejoint twitter en ', 'k tweets', 'à propos',
                'Photos', 'amis en commun']

    score = scorer.evaluate(strong_keywords, keywords)
    if score >= 50:
        return True, score
    return False, score


def look_for_ecommerce_page(scorer):
    strong_keywords = ['panier', 'deals', 'ebay', 'amazon', 'cdiscount', 'veepee', 'fnac', 'booking',
                       'carrefour', 'aliexpress', 'dealabs', 'oscaro', 'leboncoin', 'montresandco', 'basket',
                       'promos']
    keywords = ['votre panier', 'mes commandes', 'panier vide', 'mon panier', 'livraison gratuite', 'frais de livraison',
                'promos', '-50%', '-30%','-25%', 'vos commandes', 'panier', 'ventes privées', 'nouveautés', 'promotions',
                'achat', 'vente', 'achat et vente', '10€ offerts', '15€ offerts','20€ offerts', '30€ offerts',
                'offres spéciales', '30 jours pour se décider', '100€ d\'achats', 'marque', 'modèle', 'garanti',
                'codes promos', 'bons plan', 'gratuit', 'commander', 'acheter', 'vendre', 'produits', 'mes course',
                'panier', 'mon compte', 'bon plans', '25€']

    score = scorer.evaluate(strong_keywords, keywords)
    if score >= 50:
        return True, score
    return False, score


def look_for_articles_page(scorer):
    strong_keywords = ['Livraison gratuite', 'Ajouter au panier', 'Produits similaires', 'Amazon', 'Tous les produits',
                       'carrefour', 'aliexpress', 'dealabs', 'oscaro', 'leboncoin', 'trier par', 'filtrer par',
                       'Cdiscount', 'Voir votre panier', 'Frais de livraison', 'panier', 'deals', 'ebay',
                       'veepee', 'fnac', 'booking',
                       'aliexpress', 'dealabs', 'montresandco', 'basket',
                       'promos']
    keywords = ['affiner la catégorie',   '100 000 résultats pour ','En stock', '-50%', '-30%', '-25%', 'avis',
                 'En stock le ', 'En rupture de stock', 'articles recommandés', 'trier par marque', 'trier par prix'
                'nouveautés', 'promotions', 'offres de produits', 'autres vendeurs', '10€ offerts', '15€ offerts',
                '20€ offerts', '30€ offerts','offres spéciales', 'recevez le ', 'Amazon prime',
                'garanti', 'codes promos', 'bons plan', 'ventes flash', 'commander', 'acheter ce produit', 'mes course'
                , 'Prix mini', 'meilleurs ventes', 'reconditionnés' ,'produit neuf', 'livraison chez vous', 'garantie',
                'garantie 2 ans', 'caractéristiques', 'description', 'référence', 'réf', 'avis', 'commentaires',
                'avis client', 'achat vérifié', 'cet avis vous est utile', 'accessoires', 'dimensions', 'payer en 3 fois',
                'Les points forts', 'satisfait ou remboursé', 'point retrait', 'point relais', 'informations générales',
                'marque', 'nom du produit', 'observations', 'couleur', 'matières', 'satisfait ou remboursé',
                'livraison à domicile', 'expédié par', 'descriptif']

    score = scorer.evaluate(strong_keywords, keywords)
    if score >= 50:
        return True, score
    return False, score


def look_for_stream_page(scorer):
    strong_keywords = ['lestream', 'twitch', 'youtube', 'netflix', 'mycanal', 'amazon', 'dailymotion', 'rakuten.tv']
    keywords = ['lestream', 'chatting', 'jeux video', 'suivre', 's\'abonner', 'media', 'follow',
                'subscribe', 'vidéos', 'replay', 'joue à', 'spectateurs', 'live', 'commentaires', 'youtube',
                'twitch', 'netflix', 'chaîne', 'chaîne live', 'clips', 'toutes les chaines', 'programme', 'partager',
                'like', 'dislike', 'enregistrer']
    score = scorer.evaluate(strong_keywords, keywords)
    if score >= 25:
        return True, score
    return False, score


def look_for_information_page(scorer):
    strong_keywords = ['leparisien', 'mediapart', 'lefigaro', 'lequipe', 'lemonde',
                       '20minutes', 'lci', 'lexpress', 'linternaute', 'lepoint',
                       'ouest-france', 'sudouest', 'ladepeche', 'commentcamarche', 'latribune',
                       'liberation', 'nouvelobs', 'lesechos', 'news.google.com', 'rtl', 'public'
                       'europe1', 'journaldunet', 'huffingtonpost', 'lavoixdunord', 'cnews']
    keywords = ['faits divers', 'actualité', 'politique', 'économie', 'sport', 'media', 'journal', 'publié le'
                'chiffres', 'article', 'lire aussi', 'AFP', 'publié', 'société', 'culture', 'même sujet',
                'commentaire', 'abonne', 's\'abonné', 'information', 'people', 'dernières news', 'partagez'
                'commmentez', 'buzze', 'le magazine']
    score = scorer.evaluate(strong_keywords, keywords)
    if score >= 25:
        return True, score
    return False, score


def look_for_contact_page(scorer):
    strong_keywords = ['contact us', 'nous contacter', 'service client', 'contact', 'help']
    keywords = ['probleme', 'signaler', 'aide', 'question', 'Contact', 'Message', 'sujet',
                'email', 'envoyer', 'demande', 'assistance', 'address', 'service client', 'appel',
                'help', 'vous aider', 'contactez-nous', 'un conseiller', 'aide en ligne', 'Questions fréquentes',
                'service d\'assistance', 'assistance technique', 'contact us', 'contact information', 'email',
                'contact email', 'customer services']

    score = scorer.evaluate(strong_keywords, keywords)
    if score >= 50:
        return True, score
    return False, score


def look_for_legal_disclaimer_page(scorer):
    strong_keywords = ['end-user-agreement', 'license', 'copyright', 'policy', 'privacy', 'terms', 'policies', 'user-agreement']
    keywords = ['confidentialité', 'legal', 'regle', 'Sécurité', 'politique', 'condition', 'utilisation',
                'signale', 'loi', 'droit', 'protection', 'données personnelles', 'contrat', 'contenu',
                'utiliser les Services', 'violation de droits', 'PROPRIETE INTELLECTUELLE', 'DROIT D\'AUTEUR',
                'PROTECTION DES BASES DE DONNEES', 'MARQUES DEPOSEES', 'brevet', 'Conditions d\'Utilisation',
                'fournisseur de contenu', 'Protection de vos informations personnelles', 'informations personnelles',
                'third-party rights', 'policies', 'Policy Enforcement', 'agree', 'rules', 'User Agreement', 'copyright', 'end-user-agreement',
                'privacy', 'terms of use', 'jurisdiction']
    score = scorer.evaluate(strong_keywords, keywords)
    if score >= 50:
        return True, score
    return False, score


def look_for_payment_page(scorer):
    strong_keywords = ['Paiement', 'Valider le paiement', 'Acheter', 'Récapitulatif de la commande' , 'Montant total',
                       'payer avec Paypal', 'payer par carte', 'Mode de paiement', 'Visa', 'Adresse de facturation']
    keywords = ['Date de livraison estimé', 'acheté', 'acheter', 'commande', 'Ajouter un code promotionnel',
                'Numéro de carte', 'CV code', 'CVV', 'Chèques cadeaux' , 'Cadeau', 'Vérification du paiement'
                'Frais', 'date d\'expiration', 'expiration', 'Livraison standard', 'titulaire de la carte', 'prix',
                'montant', 'total', 'montant total', 'article', 'details', '€', '$', 'mode de paiement',
                'Adresse de livraison', 'date de livraison', 'expédiés par', 'Promotion appliquée', 'Paypal',
                'Compte bancaire', 'Validation du paiement', 'conditons générales de vente', 'débit']
    score = scorer.evaluate(strong_keywords, keywords)
    if score >= 25:
        return True, score
    return False, score
