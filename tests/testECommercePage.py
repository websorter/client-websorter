from unittest import TestCase

from HttpRequester import request
from Scorer import Scorer
from URLParser import look_for_ecommerce_page
from models.Task import Task


class TestPECommercePage(TestCase):
    def setUp(self):
        self.task_test = Task(1, None, None, 'null', 'null', 'null', 1)

    def test_ebay_good_deal_ecommerce_page(self):
        self.task_test.name = 'eBay bon plans'
        self.task_test.url = 'https://www.ebay.fr/deals'

        response = request('get', self.task_test.url)
        subject = 'Produits électroniques, Voitures, Vêtements, Objets de collection, Bons d&apos;achat et autres achats en ligne | eBay'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_ecommerce_page(scorer))

    def test_montresandco_ecommerce_page(self):
        self.task_test.name = 'montresandco site ecommerce'
        self.task_test.url = 'https://www.montresandco.com/'

        response = request('get', self.task_test.url)
        subject = 'Montres Homme, Femme, Enfant de marques | Montres and Co'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_ecommerce_page(scorer))



    def test_dealabs_ecommerce_page(self):
        self.task_test.name = 'dealabs site ecommerce'
        self.task_test.url = 'https://www.dealabs.com/'

        response = request('get', self.task_test.url)
        subject = 'Dealabs.com – Tous les deals, bons plans, réducs et codes promo'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_ecommerce_page(scorer))

    def test_cdiscount_ecommerce_page(self):
        self.task_test.name = 'cdiscount site e-commerce'
        self.task_test.url = 'https://www.cdiscount.com/electromenager/'

        response = request('get', self.task_test.url)
        subject = 'Cdiscount.com - N\'économisez pas votre plaisir'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_ecommerce_page(scorer))
