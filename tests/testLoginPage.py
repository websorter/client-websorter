from unittest import TestCase

from HttpRequester import request
from Scorer import Scorer
from models.Task import Task
from URLParser import look_for_login_page


class TestLoginPage(TestCase):
    def setUp(self):
        self.task_test = Task(1, None, None, 'null', 'null', 'null', 1)

    def test_google_sign_in(self):
        self.task_test.name = 'Log In Google Account'
        self.task_test.url = 'https://accounts.google.com/ServiceLogin'

        response = request('get', self.task_test.url)
        subject = 'Connexion : comptes Google'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_login_page(scorer))

    def test_spotify_login(self):
        self.task_test.name = 'Login - Spotify'
        self.task_test.url = 'https://accounts.spotify.com/en/login/'

        response = request('get', self.task_test.url)
        subject = 'Login - Spotify'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_login_page(scorer))

    def test_should_not_pass_for_sign_up_spotify_page(self):
        self.task_test.name = 'S\'inscrire - Spotify'
        self.task_test.url = ' https://www.spotify.com/ca-fr/signup'

        response = request('get', self.task_test.url)
        subject = 'S\'inscrire - Spotify'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertFalse(look_for_login_page(scorer)[0])

    def test_credit_mutuel_login_page(self):
        self.task_test.name = 'authentification de Crédit Mutuel'
        self.task_test.url = 'https://www.creditmutuel.fr/authentification.html'

        response = request('get', self.task_test.url)
        subject = 'Connexion à votre Espace Client - Crédit Mutuel'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_login_page(scorer))

    def test_login_big_blue_button(self):
        self.task_test.name = 'login bigbluebutton'
        self.task_test.url = 'https://demo.bigbluebutton.org/gl/signin'

        response = request('get', self.task_test.url)
        subject = 'BigBlueButton'

        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_login_page(scorer))

    def test_should_not_validate_sign_up_bigbluebutton(self):
        self.task_test.name = 'Sign Up bigbluebutton'
        self.task_test.url = 'https://demo.bigbluebutton.org/gl/signup'

        response = request('get', self.task_test.url)
        subject = 'BigBlueButton'

        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertFalse(look_for_login_page(scorer)[0])

    def test_login_stripe(self):
        self.task_test.name = 'login Stripe'
        self.task_test.url = 'https://dashboard.stripe.com/login'

        response = request('get', self.task_test.url)
        subject = 'Stripe : connexion'

        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_login_page(scorer))