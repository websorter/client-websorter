from unittest import TestCase

from HttpRequester import request
from Scorer import Scorer
from URLParser import look_for_stream_page
from models.Task import Task


class TestStreamPage(TestCase):
    def setUp(self):
        self.task_test = Task(1, None, None, 'null', 'null', 'null', 1)

    def test_lestreamfr_stream_page(self):
        self.task_test.name = 'LeStream.fr'
        self.task_test.url = 'https://lestream.fr/'

        response = request('get', self.task_test.url)
        subject = 'Accueil - LeStream'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_stream_page(scorer))

    def test_twitch_stream_page(self):
        self.task_test.name = 'twitch science et technology channel'
        self.task_test.url = 'https://www.twitch.tv/directory/game/Programming'

        response = request('get', self.task_test.url)
        subject = 'Science et technologie - Twitch'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_stream_page(scorer))


    def test_youtube_stream_page(self):
        self.task_test.name = 'youtube Joma tech : if programming was an anime'
        self.task_test.url = 'https://www.youtube.com/watch?v=pKO9UjSeLew'

        response = request('get', self.task_test.url)
        subject = '(102) If Programming Was An Anime - YouTube'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_stream_page(scorer))


    def test_rakuten_stream_page(self):
        self.task_test.name = 'rakuten tv'
        self.task_test.url = 'https://rakuten.tv/fr'

        response = request('get', self.task_test.url)
        subject = 'Rakuten TV - Votre cinéma à la maison'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_stream_page(scorer))



