from unittest import TestCase

from HttpRequester import request
from Scorer import Scorer
from models.Task import Task
from URLParser import look_for_sign_up_page


class TestInformationPage(TestCase):
    def setUp(self):
        self.task_test = Task(1, None, None, 'null', 'null', 'null', 1)

    def test_google_sign_up(self):
        self.task_test.name = 'Sign Up Google Account'
        self.task_test.url = 'https://accounts.google.com/signup/v2/webcreateaccount?hl=en&flowName=GlifWebSignIn&flowEntry=SignUp'

        response = request('get', self.task_test.url)
        subject = 'Create your Google Account'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_sign_up_page(scorer))

    def test_spotify_sign_up(self):
        self.task_test.name = 'Sign Up Spotify'
        self.task_test.url = 'https://www.spotify.com/fr/signup/'

        response = request('get', self.task_test.url)
        subject = 'S\'inscrire - Spotify'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_sign_up_page(scorer))

    def test_stripe_sign_up(self):
        self.task_test.name = 'Sign Up Stripe'
        self.task_test.url = 'https://dashboard.stripe.com/register'

        response = request('get', self.task_test.url)
        subject = 'Stripe: Register'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_sign_up_page(scorer))

    def test_trello_sign_up(self):
        self.task_test.name = 'Sign Up Trello'
        self.task_test.url = 'https://trello.com/fr/signup'

        response = request('get', self.task_test.url)
        subject = 'Créer un compte Trello'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_sign_up_page(scorer))

    def test_github_sign_up(self):
        self.task_test.name = 'Sign Up Github'
        self.task_test.url = 'https://github.com/join'

        response = request('get', self.task_test.url)
        subject = 'Join GitHub · GitHub'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_sign_up_page(scorer))

