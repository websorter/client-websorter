from unittest import TestCase

from HttpRequester import request
from Scorer import Scorer
from URLParser import look_for_contact_page
from models.Task import Task


class TestDisclaimerPage(TestCase):
    def setUp(self):
        self.task_test = Task(1,None,None,'null', 'null', 'null', 1)

    def test_oui_sncf_contact_page(self):
        self.task_test.name = 'Oui SNCF contact page'
        self.task_test.url = 'https://www.oui.sncf/aide/contact'

        response = request('get', self.task_test.url)
        subject = 'Contact | OUI.sncf'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_contact_page(scorer=scorer))

    def test_stripe_contact_page(self):
        self.task_test.name = 'Stripe contact page'
        self.task_test.url = 'https://stripe.com/fr/contact'

        response = request('get', self.task_test.url)
        subject = 'Stripe : Contactez-nous'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_contact_page(scorer=scorer))


    def test_credit_mutuel_contact_page(self):
        self.task_test.name = 'Credit mutuel contact page'
        self.task_test.url = 'https://www.creditmutuel.fr/fr/particuliers/nous-contacter/index.html'

        response = request('get', self.task_test.url)
        subject = 'Nous contacter | Crédit Mutuel'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_contact_page(scorer=scorer))

    def test_allibaba_contact_page(self):
        self.task_test.name = 'alibaba contact page'
        self.task_test.url = 'https://activities.alibaba.com/alibaba/contact_us_megasourcing2nd.php'

        response = request('get', self.task_test.url)
        subject = 'Contact Us'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_contact_page(scorer=scorer))
