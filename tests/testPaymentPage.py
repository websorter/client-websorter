from unittest import TestCase

from HttpRequester import request
from Scorer import Scorer
from models.Task import Task

from URLParser import look_for_payment_page


class TestPaymentPage(TestCase):

    def setUp(self):
        self.task_test = Task(1, None, None, 'null', 'null', 'null', 1)

    def test_bootstrap_snippet_payment_form(self):
        self.task_test.name = 'Exemple Bootstrap de formulaire de paiement'
        self.task_test.url = 'https://bootsnipp.com/snippets/5Mkl8'
        response = request('get', self.task_test.url)
        subject = 'Bootstrap Snippet Payment form, credit card form block bootstrap4  using HTML  Bootstrap  '
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_payment_page(scorer))


