from unittest import TestCase

from HttpRequester import request
from Scorer import Scorer
from URLParser import look_for_legal_disclaimer_page,look_for_sign_up_page
from models.Task import Task


class TestDisclaimerPage(TestCase):
    def setUp(self):
        self.task_test = Task(1,None,None,'null', 'null', 'null', 1)

    def test_spotify_end_user_agreement(self):
        self.task_test.name = 'Conditions générales d\'utilisation de Spotify'
        self.task_test.url = 'https://www.spotify.com/fr/legal/end-user-agreement/'

        response = request('get', self.task_test.url)
        subject = 'Conditions d\'utilisation'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_legal_disclaimer_page(scorer))

    def test_facebook_terms(self):
        self.task_test.name = 'Conditions d’utilisation de Facebook'
        self.task_test.url = 'https://fr-fr.facebook.com/terms'

        response = request('get', self.task_test.url)
        subject = 'Conditions d\'utilisation'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_legal_disclaimer_page(scorer))

    def test_google_terms(self):
        self.task_test.name = 'Conditions d’utilisation de Google'
        self.task_test.url = 'https://policies.google.com/terms?hl=fr'

        response = request('get', self.task_test.url)
        subject = 'Conditions d\'utilisation de Google'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_legal_disclaimer_page(scorer))

    def test_amazon_terms(self):
        self.task_test.name = 'Conditions d’utilisation de Amazon'
        self.task_test.url = 'https://www.amazon.fr/gp/help/customer/display.html?nodeId=201909000'

        response = request('get', self.task_test.url)
        subject = 'Amazon.fr Aide: CONDITIONS D\'UTILISATION ET GENERALES DE VENTE'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_legal_disclaimer_page(scorer))

    def test_microsoft_services_terms(self):
        self.task_test.name = 'Conditions d’Utilisation du Service de Microsoft'
        self.task_test.url = 'https://www.microsoft.com/fr-fr/mobile/service-terms'

        response = request('get', self.task_test.url)
        subject = 'Conditions d’Utilisation du Service'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_legal_disclaimer_page(scorer))

    def test_microsoft_azure_terms_of_use(self):
        self.task_test.name = 'Conditions d’Utilisation du Site Web Microsoft Azure'
        self.task_test.url = 'https://azure.microsoft.com/fr-fr/support/legal/website-terms-of-use/'

        response = request('get', self.task_test.url)
        subject = 'Conditions d’Utilisation du Site Web Microsoft Azure'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_legal_disclaimer_page(scorer))

    def test_ebay_terms_of_use(self):
        self.task_test.name = 'Conditions d’Utilisation d\'ebay'
        self.task_test.url = 'https://www.ebay.com/help/policies/member-behaviour-policies/user-agreement?id=4259'

        response = request('get', self.task_test.url)
        subject = 'User Agreement | eBay'
        with open('test.html', 'w') as file:
            file.write(response.text)
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_legal_disclaimer_page(scorer))


