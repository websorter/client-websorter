from unittest import TestCase

from HttpRequester import request
from Scorer import Scorer
from URLParser import look_for_profile_page
from models.Task import Task


class TestProfilePage(TestCase):
    def setUp(self):
        self.task_test = Task(1, None, None, 'null', 'null', 'null', 1)

    def test_facebook_github_profile_page(self):
        self.task_test.name = 'Github Facebook page'
        self.task_test.url = 'https://fr-fr.facebook.com/GitHub/'

        response = request('get', self.task_test.url)
        subject = 'GitHub - Accueil | Facebook'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_profile_page(scorer))

    def test_linked_in_profile_accueil(self):

        self.task_test.name = 'Accueil linkedin page'
        self.task_test.url = 'https://fr.linkedin.com/company/accueil'

        response = request('get', self.task_test.url)
        subject = 'GitHub - Accueil | Facebook​'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_profile_page(scorer))

    def test_gitlab_amaubert_jee_project(self):
        self.task_test.name = 'Gitlab amaubert where is my home API'
        self.task_test.url = 'https://gitlab.com/amaubert/back-jee-project-group-3'

        response = request('get', self.task_test.url)
        subject = 'Allan Maubert / Where-is-my-home-api · GitLab​'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_profile_page(scorer))

    def test_instagram_france_fr(self):
        self.task_test.name = 'instagram_france_fr profile page'
        self.task_test.url = 'https://www.instagram.com/francefr/?hl=fr'

        response = request('get', self.task_test.url)
        subject = 'France.fr (@francefr) • Photos et vidéos Instagram​'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_profile_page(scorer))

    def test_twitter_rer_a_profile_page(self):
        self.task_test.name = 'RER A twitter profile page'
        self.task_test.url = 'https://twitter.com/rer_a?lang=fr'

        response = request('get', self.task_test.url)
        subject = 'RER A (@RER_A) / Twitter​'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_profile_page(scorer))