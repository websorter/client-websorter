from unittest import TestCase

from HttpRequester import request
from Scorer import Scorer
from URLParser import look_for_information_page
from models.Task import Task


#console.log(document.querySelector("head > title"))
class TestInformationPage(TestCase):
    def setUp(self):
        self.task_test = Task(1, None, None, 'null', 'null', 'null', 1)

    def test_leparisien_faits_divers(self):
        self.task_test.name = 'Faits divers LeParisien'
        self.task_test.url = 'http://www.leparisien.fr/faits-divers/'
        response = request('get', self.task_test.url)
        subject = 'Faits Divers : Toute l\'actualité des faits divers sur Le Parisien'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_information_page(scorer))

    def test_lefigaro_politics(self):
        self.task_test.name = 'Politique LeFigaro'
        self.task_test.url = 'https://www.lefigaro.fr/politique'

        response = request('get', self.task_test.url)
        subject = 'Le Figaro - Politique - L\'actualité du gouvernement et de l\'opposition'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_information_page(scorer))

    def test_france24_article(self):
        self.task_test.name = 'Article de France24'
        self.task_test.url = 'https://www.lagazettedescommunes.com/679358/une-serie-de-guides-pour-aider-au-deconfinement-de-la-culture/'

        response = request('get', self.task_test.url)
        subject = 'En direct : Emmanuel Macron et Angela Merkel annoncent leur plan pour relancer l\'économie de l\'UE'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_information_page(scorer))

    def test_3(self):
        self.task_test.name = 'Article de France24'
        self.task_test.url = 'https://www.1jour1actu.com/monde/'

        response = request('get', self.task_test.url)
        subject = 'En direct : Emmanuel Macron et Angela Merkel annoncent leur plan pour relancer l\'économie de l\'UE'
        scorer = Scorer(response.text, self.task_test.url, subject)
        self.assertTrue(look_for_information_page(scorer))