import json
import unittest

import HttpRequester
import configparser

from Scorer import Scorer
from URLParser import DocumentFinder, look_for_home_page, look_for_login_page, look_for_sign_up_page, \
    look_for_profile_page, look_for_ecommerce_page, look_for_articles_page, look_for_stream_page, look_for_payment_page

from requests import Response

from ErrorType import ErrorType
from models.Task import Task
from models.User import User
from bs4 import BeautifulSoup


class TestURLParser(unittest.TestCase):
    def setUp(self):

        self.response_test = Response()
        self.task_test = Task(1, "test", "https://www.google.fr/", "test_date", "test_date", "result", 1)
        self.task_test.url = "https://www.google.fr/"
        self.subject = 'Google'
        self.html_code = "<!DOCTYPE HTML PUBLIC><HTML><HEAD><TITLE>html test page</TITLE></HEAD><BODY></BODY></HTML>"

    def test_check_response_redirection(self):
        self.response_test.url = "google.fr"
        self.response_test.status_code = 200
        document_finder = DocumentFinder(self.task_test, self.subject)
        self.assertEqual(True, document_finder.check_response(self.response_test))

    def test_check_response_bad_status_code(self):
        self.response_test.url = "https://www.google.fr/"
        self.response_test.status_code = 404
        document_finder = DocumentFinder(self.task_test, self.subject)
        self.assertEqual(ErrorType.BadRequest, document_finder.check_response(self.response_test))

    def test_look_for_home_page(self):
        scorer = Scorer("", self.task_test.url, "")
        self.assertTrue(look_for_home_page(scorer))

    def test_look_for_login_page(self):
        self.task_test.url = "https://www.google.fr/login"
        scorer = Scorer("", self.task_test.url, "")
        self.assertTrue(look_for_login_page(scorer))

    def test_look_for_profile_page(self):
        self.html_code = self.html_code[:76] + "<a>Votre profil</a>" + self.html_code[76:]
        scorer = Scorer(self.html_code, self.task_test.url, "")
        self.assertTrue(look_for_profile_page(scorer))

    def test_look_for_e_commerce_page(self):
        self.html_code = self.html_code[:76] + "<p>Votre panier est vide</p>" + self.html_code[76:]
        scorer = Scorer(self.html_code, self.task_test.url, "")
        self.assertTrue(look_for_ecommerce_page(scorer))

    def test_look_for_articles_page(self):
        self.html_code = self.html_code[:76] + "1234 résultats" + self.html_code[76:]
        scorer = Scorer(self.html_code, self.task_test.url, "")
        self.assertTrue(look_for_articles_page(scorer))

    def test_look_for_stream_page(self):
        self.html_code = self.html_code[:76] + "Chat du live" + self.html_code[76:]
        scorer = Scorer(self.html_code, self.task_test.url, "")
        self.assertTrue(look_for_stream_page(scorer))

    def test_find_subject(self):
        document_finder = DocumentFinder(self.task_test, [self.subject])
        soup = BeautifulSoup(self.html_code, features="html.parser")
        self.assertEqual("html test page", document_finder.find_page_subject(soup))

    def test_look_for_payment_page(self):
        self.html_code = self.html_code[:76] + "<p>Validation du paiement</p>" + self.html_code[76:]
        scorer = Scorer(self.html_code, self.task_test.url, "")
        self.assertTrue(look_for_payment_page(scorer))


class TestHttpRequester(unittest.TestCase):
    def setUp(self):
        self.conf = configparser.ConfigParser()
        self.conf.read("../.env")

    @unittest.skip
    def test_get_hello(self):
        self.assertEqual("Hello !", HttpRequester.request("get", self.conf.get("routes", "API_URL") + "/hello").text)

    @unittest.skip
    def test_create_account(self):
        user_test = User("user_test@user_test.com")
        self.assertEqual(201, HttpRequester.request("post", self.conf.get("routes", "API_ACCOUNT"),
                                                    json.dumps(user_test.serialize())).status_code)
