import json


class Routes:

    def __init__(self, base_url):
        self.base_url = base_url
        with open('routes.json', 'r') as routes_file:
            self.api_routes = json.load(routes_file)

    def api_hello(self):
        uri = self.api_routes["HELLO"]["prefix"]
        return f'{self.base_url}{uri}'

    def api_login(self):
        uri = self.api_routes["LOGIN"]["prefix"]
        return f'{self.base_url}{uri}'

    def api_task_available(self):
        uri = f'{self.api_routes["TASK"]["prefix"]}{self.api_routes["TASK"]["routes"]["available"]["prefix"]}'
        return f'{self.base_url}{uri}'

    def api_task(self):
        uri = self.api_routes["TASK"]["prefix"]
        return f'{self.base_url}{uri}'

    def api_task_update(self, task_id):
        uri = self.api_routes["TASK"]["prefix"]
        return f'{self.base_url}{uri}/{task_id}'
