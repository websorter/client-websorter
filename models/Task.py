import re


class Task:
    regex = re.compile(
        r'^(?:http|ftp)s?://'
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
        r'localhost|'
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
        r'(?::\d+)?'
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

    def __init__(self, id, name, url, createdDate, executionDate, result, accountId):
        self.id = id
        self.name = name
        self.url = url
        self.createdDate = createdDate
        self.executionDate = executionDate
        self.result = result
        self.accountId = accountId

    def __str__(self):
        return self.id + " " + self.name + " " + self.url + " " + self.createdDate + " " + self.executionDate == None if "" else self.executionDate + " " + "self.result" + " " + self.accountId == None if "" else self.accountId

    def serialize(self):
        return {"id": self.id, "name": self.name, "url": self.url, "createdDate": self.createdDate, "executionDate": self.executionDate, "result": self.result.serialize(), "accountId": self.accountId}

    def verify(self):
        if self.wrong_name():
            return "Incorrect task name : " + self.name
        elif self.wrong_url():
            return "Incorrect url : " + self.url
        else:
            return False

    def wrong_name(self):
        return False

    def wrong_url(self):
        return not re.match(self.regex, self.url)
