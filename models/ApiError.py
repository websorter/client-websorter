

class ApiError:

    def __init__(self, status, error):
        self.status = status
        self.error = error

    def serialize(self):
        return {'status': self.status, 'error': self.error }