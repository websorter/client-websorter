class Result:
    def __init__(self, success, type, subject, error, progression):
        self.success = success
        self.type = type
        self.subject = subject
        self.error = error
        self.progression = progression

    def serialize(self):
        return {"success": self.success, "type": self.type, "subject": self.subject, "error": self.error, "progression": self.progression}