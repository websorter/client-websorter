class User:
    def __init__(self, email):
        self.email = email

    def serialize(self):
        return {"email": self.email}
