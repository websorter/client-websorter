import json
import sys
from datetime import datetime
import HttpRequester
import requests
import argparse
import time
import pytz

from DocumentType import DocumentType
from ErrorType import ErrorType
from Routes import Routes
from URLParser import DocumentFinder
from models.Result import Result
from models.Task import Task
from models.AppLogin import AppLogin
from configuration import Config


class Runner:
    def __init__(self, args):
        self.routes = Routes(base_url=Config.api_base_url())
        self.json_encoder = json.JSONEncoder()
        self.headers = {}
        self.args = args
        print("interval between requests set to " + str(self.args.seconds) + " seconds")
        self.args.seconds = int(self.args.seconds)

    def ping_api(self):
        try:
            response = HttpRequester.request("get", self.routes.api_hello())
            if response.status_code != 200:
                raise Exception("API is down or unreachable")
            return response
        except requests.exceptions.RequestException:
            raise Exception("API is down or unreachable")

    def login(self):
        app_login = AppLogin(self.args.login, self.args.password)
        response = HttpRequester.request("post", self.routes.api_login(),
                                         self.json_encoder.encode(app_login.serialize()))
        if response.status_code != 204:
            print('Login Failed',file=sys.stderr)
            raise Exception(response.content)
        bearer_token = response.headers.get('Authorization')
        self.headers = {'Authorization': bearer_token}

    def ask_new_available_task(self):
        response = HttpRequester.request("get", self.routes.api_task_available(),
                                         headers=self.headers)

        if response.status_code == 404:
            print("No tasks found")
            return None
        elif response.status_code != 200:
            print("request failed : " + str(response.status_code))
            print("request failed : " + response.text)
            return None
        task = Task(response.json()["id"], response.json()["name"], response.json()["url"],
                    response.json()["createdDate"],
                    response.json()["executionDate"], response.json()["result"], response.json()["accountId"])
        return task

    def run(self):
        time_running = self.args.seconds * 20

        while True:
            #Update Routes in config file possible at Runtime
            self.routes = Routes(base_url=Config.api_base_url())

            print("asking API for new tasks...")
            task = self.ask_new_available_task()

            if task is None:
                time.sleep(self.args.seconds)
                time_running -= self.args.seconds
                continue
            subject = ["No subject"]

            if task.verify():
                print(task.verify())
            else:
                #TODO should update here for the executionDate
                #TODO with a Result at 0% progress
                task.createdDate = datetime.strptime(task.createdDate, '%Y-%m-%dT%H:%M:%S.%f%z').isoformat()
                task.executionDate = str(datetime.now(pytz.utc).isoformat())
                # print(task.createdDate)
                # print(task.executionDate)

                document_finder = DocumentFinder(task, subject)
                result = document_finder.identify()
                # result = page_result(task_status)
                if ErrorType.is_error(result):
                    print('Type de document trouvé : ', result.value)
                    task.result = Result(False, DocumentType.NotFoundPage.value, subject[0],
                                         result.value, 0)
                elif DocumentType.is_document_type(result):
                    task.result = Result(True, result.value, subject[0], "null", 100)
                else:
                    task.result = Result(False, DocumentType.UnidentifiedPage.value, subject[0], ErrorType.Unidentified.value, 0)
                update_result = HttpRequester.request("put", self.routes.api_task_update(task.id) ,
                                                      json.dumps(task.serialize()),
                                                      headers=self.headers)
                if update_result.status_code == 200:
                    print("Task updated successfully")
                else:
                    print("Failed to update task (" + str(update_result.status_code) + ") : " + update_result.text)

            time.sleep(self.args.seconds)
            time_running -= self.args.seconds


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='WebSorter Runner .')
    parser.add_argument('--login', type=str, required=True,
                        help='login of the user')
    parser.add_argument('--password', type=str,required=True,
                        help='password of the user')
    parser.add_argument('--seconds', type=str, required=True,
                        help='interval of second between each ask request task')
    args = parser.parse_args()

    runner = Runner(args)
    runner.ping_api()
    runner.login()
    runner.run()
