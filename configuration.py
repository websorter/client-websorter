import os
from dotenv import load_dotenv

load_dotenv(".env")


class Config:

    @staticmethod
    def api_base_url():
        return os.environ['API_URL']
