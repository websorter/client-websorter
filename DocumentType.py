from enum import Enum


class DocumentType(Enum):
    UnidentifiedPage = 'Failed to identify'
    NotFoundPage = 'NotFound Page'
    WelcomePage = 'Welcome Page'
    SignUpPage = 'SignUp Page'
    LoginPage = 'Login Page'
    ProfilePage = 'Profile Page'
    ECommercePage = 'E-Commerce Page'
    ArticlesPage = 'Articles Page'
    StreamPage = 'Stream Page'
    InformationPage = 'Information Page'
    ContactPage = 'Contact Page'
    LegalDisclaimerPage = 'Legal Disclaimer Page'
    PaymentPage = 'Payment Page'

    @staticmethod
    def is_document_type(object):
        return isinstance(object, DocumentType)



